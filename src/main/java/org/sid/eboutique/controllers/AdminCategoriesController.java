package org.sid.eboutique.controllers;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.sid.eboutique.entities.Categorie;
import org.sid.eboutique.metier.IAdminCategoriesMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/adminCat") //tt les liens pour aller vers la gestion commence par /adminCat
public class AdminCategoriesController implements HandlerExceptionResolver { // impl�menter un interface HandlerExceptionResolver pour g�rer les exceptions ;une m�thode va etre ajouter qui est resolveException
	@Autowired //demander a spring au moment de d�marage de chercher un objet qui implemente cette interface IAdminCategoriesMetier et de l'associer 
private IAdminCategoriesMetier metier; 
	
	
	@RequestMapping(value="/index")
	public String index(Model model){
		model.addAttribute("categorie", new Categorie());
		model.addAttribute("categories", metier.listCategories());
		return"categories";
		}
	
	
	@RequestMapping(value="/saveCat") // si on fait pas / c'est pas un prob 
	public String saveCat(@Valid Categorie c,BindingResult bindingResult,Model model,MultipartFile file) throws Exception{ // BindingResult bindingResult les erreurs de validation et MultipartFile file pour upload ms on doit ajouter des d�pendance qu'on a ajout� ds pom.xml et on doit configurer la taille max du fichier dans servlet-context.xml on ajoute une d�claration 
		//@valid indiquer que la validation de la cat�gorie doit etre faite par exemple est ce la taille est correcte ect.... si il a prob il affiche message 
		if(bindingResult.hasErrors()){
			model.addAttribute("categories", metier.listCategories());
			return"categories";
			}
		if(!file.isEmpty()){
			BufferedImage bi = ImageIO.read(file.getInputStream());//pour savoir est ce que c'est une photo sinon il va g�rer une exception 
			c.setPhoto(file.getBytes());
			c.setNomPhoto(file.getOriginalFilename());
		}
		if(c.getIdcategorie()!=null){
				if(file.isEmpty()){
				Categorie cat=metier.getCategorie(c.getIdcategorie());
				c.setPhoto(cat.getPhoto());	}			
			
			metier.modifierCategorie(c);
		}
		else
		metier.ajouterCategorie(c);
		model.addAttribute("categorie", new Categorie());
		model.addAttribute("categories", metier.listCategories());
		return"categories";
	}
	
	
		@RequestMapping(value="suppCat")
		public String supp(Long idCat, Model model){
			metier.supprimerCategrorie(idCat);
			model.addAttribute("categorie", new Categorie());
			model.addAttribute("categories", metier.listCategories());
			return"categories";
			}
		
		

		@RequestMapping(value="editCat")
		public String edit(Long idCat, Model model){
			Categorie c =metier.getCategorie(idCat);
			model.addAttribute("categorie", c);
			model.addAttribute("categories", metier.listCategories());
			return"categories";
			}
		
	
	@RequestMapping(value="photoCat", produces=MediaType.IMAGE_JPEG_VALUE) // produces=MediaType.IMAGE_JPEG_VALUE je dit que je retourne pas du text ms j'indique que c'est une image jpeg
	@ResponseBody //je retourne directe le resultat ds le corps de la r�ponse 
	public byte[] photCat(Long idCat) throws IOException{
		Categorie c=metier.getCategorie(idCat);
		return IOUtils.toByteArray(new ByteArrayInputStream(c.getPhoto())); // c.getPhoto() c'est un tableau de byte 
	}

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object arg2,
			Exception ex) { // a chaque fois il a une exception ce resolver va etre appel�
	ModelAndView mv = new ModelAndView(); // un objet de type ModelAndView de spring 
	mv.addObject("categorie", new Categorie());
	mv.addObject("categories", metier.listCategories());
	mv.addObject("exception", ex.getMessage());
	mv.setViewName("categories");
		return mv;
	}

}
