<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<head>
<a href="<c:url value="/j_spring_security_logout" />">Logout</a>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/resources/css/style1.css">
</head>
<div class="errors">
${exception}
</div>
<div id="formCat" class="cadre">
<f:form modelAttribute="categorie" action="saveCat" method="post" enctype="multipart/form-data"> <!-- categorie howa model li jay mn controlleur -->
<table class="tab1">
<tr><td>ID Catégorie</td><td><f:input path="idcategorie"/></td><td><f:errors path="idcategorie" cssClass="errors"></f:errors></td></tr> <!--  path=" " khass ikoun b7al smit les attribut li f la classe ici on a idcategorie -->
<tr><td>Nom Catégorie</td><td><f:input path="nomCategorie"/></td><td><f:errors path="nomCategorie" cssClass="errors"></f:errors></td></tr>
<tr><td>Description</td><td><f:textarea path="description"/></td><td><f:errors path="description" cssClass="errors"></f:errors></td></tr>
<tr><td>Photo</td><td><c:if test="${categorie.idcategorie!=null}"><img src="photoCat?idCat=${categorie.idcategorie}" height="100" width="150" /></c:if></td><td><input type="file" name="file"/></td><td></td></tr>
<tr><td><input type="submit" value="Save" /></td></tr>
</table>
</f:form>
</div>

<div id="tabCategories" class="cadre">
<table class="tab1">
<tr><th>ID Catégorie</th><th>Nom Catégorie</th><th>Description</th><th>Photo</th><th>Supprimer</th><th>Editer</th></tr>
<c:forEach items="${categories}" var="cat">
<tr>
<td>${cat.idcategorie}</td>
<td>${cat.nomCategorie}</td>
<td>${cat.description}</td>
<td><img src="photoCat?idCat=${cat.idcategorie}" height="100" width="150" /></td>
<td><a href="suppCat?idCat=${cat.idcategorie}"/>Supprimer</td>
<td><a href="editCat?idCat=${cat.idcategorie}"/>Edit</td>
</tr>
</c:forEach>


</table>



</div>